
public class CompareTwoElements {

	public static void main(String[] args) {

		short var1 = -2, var2 = 5;

		if (var1 > var2) {
			System.out.println(var1 + " is larger than " + var2);
			System.out.println(var1 + " is the largest");
			System.out.println(var2 + " is the smallest");
		} else if (var1 < var2) {
			System.out.println(var2 + " is larger than " + var1);
			System.out.println(var2 + " is the largest");
			System.out.println(var1 + " is the smallest");
		} else
			System.out.println("Both variables are the same");

		if (var1 % 2 == 1)
			System.out.println("The number " + var1 + " is odd");
		else
			System.out.println("The number " + var1 + " is even");
		if (var2 % 2 == 1)
			System.out.println("The number " + var2 + " is odd");
		else
			System.out.println("The number " + var2 + " is even");

		if (var1 < 0)
			System.out.println("Number " + var1 + " is negative");
		else if (var1 > 0)
			System.out.println("Number " + var1 + " is positive");

		if (var2 < 0)
			System.out.println("Number " + var2 + " is negative");
		else if (var2 > 0)
			System.out.println("Number " + var2 + " is positive");

		if (var1 > 100)
			System.out.println("Number " + var1 + " is greater than 100");

		if (var2 > 100)
			System.out.println("Number " + var2 + " is greater than 100");

	}
}
