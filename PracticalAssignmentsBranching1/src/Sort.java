import java.util.Scanner;

public class Sort {

	public static void main(String[] args) {

		Scanner myScanner = new Scanner(System.in);

		System.out.println("Enter the first number:");
		double number1 = Double.parseDouble(myScanner.nextLine());

		System.out.println("Enter the second number:");
		double number2 = Double.parseDouble(myScanner.nextLine());

		System.out.println("Enter the third number:");
		double number3 = Double.parseDouble(myScanner.nextLine());

		if (number1 > number3) {
			if (number3 < number2 && number2 > number1)
				System.out.println(number2 + " " + number1 + " " + number3);
			else if (number3 > number2)
				System.out.println(number1 + " " + number3 + " " + number2);
			else
				System.out.println(number1 + " " + number2 + " " + number3);
		} else {
			if (number1 > number2)
				System.out.println(number3 + " " + number1 + " " + number2);
			else if (number3 > number2)
				System.out.println(number3 + " " + number2 + " " + number1);
			else
				System.out.println(number2 + " " + number3 + " " + number1);
		}

		myScanner.close();
	
	}

}
