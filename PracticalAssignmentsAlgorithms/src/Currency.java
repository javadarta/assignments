import java.util.Scanner;
import java.lang.Math;

public class Currency {

	public static void main(String[] args) {

		System.out.println("Enter value of USD:");
		Scanner myScanner = new Scanner(System.in); // "Conversion rate: 1 USD = 0.82 EUR" "Conversion rate: 1 EUR = 1.22 USD");
		int value = Integer.parseInt(myScanner.nextLine());
		System.out.println("In EUR");
		System.out.println(Math.abs(value / 1.22));

		System.out.println("Enter value of EUR:");
		value = Integer.parseInt(myScanner.nextLine());
		System.out.println("In USD");
		System.out.println(Math.abs(value / 0.82));

		myScanner.close();

		
	}

}
