package cakeAndMaryAlgorithm;

import java.util.Scanner;

public class cake {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner input = new Scanner(System.in);
		System.out.println("What do i need to bake a cake?");
		System.out.println("480g egg whites (we used Two Chicks egg whites from a carton)\r\n"
				+ "400g white caster sugar\r\n"
				+ "2 tbsp lemon juice\r\n"
				+ "1 tsp vanilla extract\r\n"
				+ "140g plain white flour , sifted twice\r\n"
				+ "For the meringue icing\r\n"
				+ "2 large egg whites\r\n"
				+ "225g white caster sugar\r\n"
				+ "splash of vanila extract\r\n"
				+ "pinch cream of tartar\r\n"
				+ "For the strawberry ganache\r\n"
				+ "100g white chocolate , finely chopped\r\n"
				+ "50ml double cream\r\n"
				+ "50g strawberries , chopped\r\n"
				+ "red food colouring");
		System.out.println("What to do?");
		System.out.println("STEP 1\r\n"
				+ "Heat oven to 180C/160C fan/gas 4. Put the egg whites and sugar in a stand mixer and begin to beat on a low speed for a few mins, turn the mixer up and wait another couple of mins before adding the lemon juice, vanilla and a pinch of salt. Turn the mixer up again and whisk on high until you have a thick, soft, shiny meringue with a peak that flops over.\r\n"
				+ "\r\n"
				+ "STEP 2\r\n"
				+ "Fold in the flour with a spatula until there are no lumps left, then scrape the mixture into a 25cm angel cake tin. Bake for 40-45 mins or until the cake is puffed and lightly browned and a skewer comes out clean. Turn the tin upside down � if it doesn�t have its own legs, then invert it onto four water glasses or tins of food exactly the same size. Leave to cool completely.\r\n"
				+ "\r\n"
				+ "STEP 3\r\n"
				+ "Run a palette knife around the tin to loosen the cake and gently let it drop out, then set aside on a board or cake stand.\r\n"
				+ "\r\n"
				+ "STEP 4\r\n"
				+ "To make the meringue icing, put the egg whites, sugar, vanilla, pinch of cream of tartar and 100ml water in a large heatproof bowl set over a pan of simmering water and beat it on high speed with an electric whisk for 15 mins or until thick and shiny and standing up in stiff peaks. Ice the cake straight away as the icing will stiffen as it sets. Spread over using a palette knife or use a piping bag to pipe on swirls.\r\n"
				+ "\r\n"
				+ "STEP 5\r\n"
				+ "Put the iced cake in the fridge while you make the strawberry ganache. Put the white chocolate in a small heatproof bowl and set aside. Put the cream and strawberries in a saucepan over a medium heat and cook, stirring all the time, until the strawberries have broken down and the cream starts to simmer. Quickly pour the hot cream mixture over the white chocolate and leave it, without stirring, for 1 min, then add the food colouring and mix until smooth.\r\n"
				+ "\r\n"
				+ "STEP 6\r\n"
				+ "Push through a sieve into a jug (to get rid of the strawberry seeds) then, while it�s still runny, pour it over the top of the cake and let it drizzle down the sides. Put the cake back in the fridge for 10-20 mins to set. Just before serving (particularly if it�s a hot day), top with the strawberries and your choice of decorations.");
		System.out.println("What now?");
		System.out.println("Enjoy the Cake");
		
		
		



 }
}
