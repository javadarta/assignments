import java.util.Scanner;

public class celsiusToKelvin {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Enter Celsius:");
		double celsius = input.nextDouble();
		final double K = 273.15;
		double kelvin = celsius + K;
		System.out.println("Kelvin:");
		System.out.println(celsius + K + "K");

		System.out.println("Enter Kelvin:");
		kelvin = input.nextDouble();
		celsius = kelvin - K;
		System.out.println("Celsius:");
		System.out.println(kelvin - K);

	
		input.close();

	}

}
