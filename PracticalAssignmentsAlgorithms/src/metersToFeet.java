import java.util.Scanner;

public class metersToFeet {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); // Create a Scanner object

		final double METERS_PER_FOOT = 0.305; // Create constant value

		System.out.println("Enter a value for feet: ");// Prompt user to enter a number in feet
		double feet = input.nextDouble();

		double meters = feet * METERS_PER_FOOT; // Convert feet into meters

		System.out.println(feet + " feet is " + meters + " meters"); // Display results

		System.out.println("Enter a value for meters: "); // Prompt user to enter a number in meters
		meters = input.nextDouble();

		feet = meters / METERS_PER_FOOT; // Convert meter into feet

		System.out.println(meters + " feet is " + feet + " meters"); // Display results

		input.close();

	}
}

